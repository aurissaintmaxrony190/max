- Readme : Besoin de plus d'explicitation / de documentation  :
 Qu'est-ce qu'on va trouver dans le répertoire ? qu'est-ce qu'on attend de l'apprenant  ?
quel process on imagine dans l'utilisation de ces fiches d'activité en fonction de leur type ou statut. ?

- Besoin de plus de structuration et peut-être mieux formaliser nos metadata / tags pour mieux structurer les documents :
Quelques pistes pour illustrer

Pour chaque fiche on pourrait avoir :
- tags sur les thématiques
- tags sur les objectifs de l'activité (plus compliqué)
- tags sur les situations pédagogiques
- tags sur les publics : fiche professeur, fiche exemple eleve
- etc ...

ou Faire un modèle par fiche pour typer chaque fiche  ?

- Fiche activité Modèle
- Fiche activité à compléter
- Fiche activité à analyser
- Fiche activité à proposer (definir le circuit avec le où et le comment partager et comment organiser le retour sur ce partage ) etc ...
  
Peut-être aussi , définir des questions types pour chacune des fiches qu' Isabelle a eu la bonne idée
de proposer que cela se fasse dans les quiz notés  ... 