
[TOC]

### Quelles compétences professionnelles mises en oeuvre ici ?

Les activités dans ce bloc permettent de mobiliser les [compétences professionnelles des professeur·e·s](https://www.education.gouv.fr/bo/13/Hebdo30/MENE1315928A.htm) de manière transversale aux quatre étapes proposées :

1. Maîtriser les savoirs disciplinaires et leur didactique
2. Maîriser la langue française dans le cadre de son enseignement
3. Construire, mettre en oeuvre et animer des situation d'enseignement et d'apprentissage prenant en compte la diversité des élèves
4. Organiser et assurer un mode de fonctionnement du groupe favorisant l'apprentissage et la socialisation des élèves
5. Évaluer les progrès et les acquisitions des élèves

Nous avons justement décomposé ces compétences en quatre parties comme vu ci-dessus. 

### Quelle est la vue d'ensemble de cette progression ?

Regardons étape par étape ce qu'il y aura à faire et ce que nous y gagnerons

#### Penser - Concevoir - Élaborer

Dans cette partie, il s'agira de créer ou _emprunter_ une ressource élève et d'en donner une description. Voici le menu:

- Au niveau 1 nous allons _utiliser_ une activité existante
  - Identifier les objectifs (connaissances et compétences) d'une activité
  - Identifier les pré-requis d'une activité
  - Juger si une activité est appropriée ou pas
  - Adapter cette activité
- Au niveau 2 nous allons créer notre propre activité
  - Construire une activité élève de compréhension en utilisant des exercices appropriés au concept (en s'inspirant d'une activité existante ou à partir d'une idée originale)
  - Construire une activité élève de production (en s'inspirant d'une activité existante ou à partir d'une idée originale)
  - Construire une séquence

#### Mettre en oeuvre - Animer

Décrire, par quelques méta-données, une activité élève est très précieux pour formaliser ce qui va être fait, mais aussi partager ou valider une activité.

Ce n'est évidemment pas suffisant pour mener à bien une séance de cours. Ici, nous nous penchons sur la mise en oeuvre, en classe, d'une activité. Nous allons par exemple nous demander :

- Pourquoi faire les étapes dans tel ordre ? Quand donner la parole aux élèves ? Quand la reprendre pour avancer ? Quelles interactions entre les élèves ? Quels sont les passages délicats à bien calculer ? 

On passe donc ici à la mise en place d'un scénario de séquence en soulevant et anticipant les questions qui vont se poser.

- Au niveau 1 nous allons _utiliser_ une activité existante
  - Identifier et critiquer le scénario d'une séquence
  - Scénariser une séquence existante
- Au niveau 2 nous allons reprendre l'activité _créée_ par nous et scénariser entièrement une séance, en précisant notamment :
  - les différentes phases, durée,
  - l'organisation du tableau, écran, papier
  - l'organisation pédagogique (seul, binôme, groupe...)
  - les attendus
  - les interactions
  - l'activité du professeur
  - la place de la trace écrite

#### Accompagner l'individu et le collectif

Une fois notre activité choisie, créée, sa mise en oeuvre soigneusement orchestrée, nous pouvons accompagner plus avant les élèves : ceux et celles qui ont pris de l'avance, ceux et celles qui semblent en diffculté, l'ensemble de la classe en proposant par exemple un réinvestissement des apprentissages lors d'un projet.

Cette partie se structure différement des précédentes, nous ne sommes plus dans une démarche de "modélisation" mais de propositions concrètes pour faire face aux défis usuels qui peuvent se poser. Par exemple :

- Proposer une activité de remédiation pour des élèves en difficulté
- Proposer une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance
- Proposer un projet en précisant :
    - les connaissances travaillées
    - le format pédagogique (durée, seul, binôme, groupe...)
    - les étayages pour assurer une progression des élèves
    - le calendrier (points d'étapes, rendus intermédiaires...)

#### Observer - Analyser - Évaluer

Mesurer que les apprentissages ont portés leur fruits, rendre compte à l'institution et aux familles : voilà ce que nous vous proposons de réaliser dans ce quatrième module. Nous revenons à un travail de conception et allons aborder les points suivants :

- Proposer un ensemble d'évaluations (courte en classe, devoir maison etc.) sur une ou plusieurs activités vues précédemment
- Proposer une évaluation conséquente type _Bac blanc_ sur une partie conséquente du programme   
- Proposer une grille d'évaluation pour un projet
- Proposer une présentation et des indicateurs pour l'institution et la famille
